FROM node:latest

WORKDIR /usr/src/app
COPY yarn.lock package.json ./
RUN yarn install && yarn run build
COPY . .
CMD ["yarn", "run", "start"]
