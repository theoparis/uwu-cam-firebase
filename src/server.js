// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs

import path from "path";
import express from "express";
import favicon from "serve-favicon";
import http from "http";
import fs from "fs";
import pugStatic from "express-pug-static";
import vhost from "@tryghost/vhost-middleware";
import { useSite } from "./routes/sites";
import multer from "multer";

module.exports = (rawArgs) => {
    const uploadsDir = path.join(__dirname, "..", "uploads");
    if (!fs.existsSync(uploadsDir)) fs.mkdirSync(uploadsDir);

    const app = express();

    const storage = multer.diskStorage({
        destination: uploadsDir,
        filename: (req, file, cb) =>
            cb(
                null,
                `${file.fieldname}.${Date.now()}${path.extname(
                    file.originalname
                )}`
            ),
    });

    // Check file type
    const checkFileType = (file, cb) => {
        // Allowed exts
        // TODO: env variable seperated by comma
        const allowedTypes = /jpeg|jpg|png|gif|txt|js/;

        // Check
        const extname = allowedTypes.test(path.extname(file.originalname));
        // Check mime type
        const mimetype = allowedTypes.test(file.mimetype);

        if (mimetype && extname) return cb(null, true);
        else return cb("Error: Images and Code Only");
    };

    const upload = multer({
        storage,
        limits: {
            fileSize: 5 * 1000000,
        },
        fileFilter: (_, file, cb) => checkFileType(file, cb),
    }).single("image");

    app.use(
        "/node_modules",
        express.static(path.join(__dirname, "..", "node_modules"))
    );

    // favicon
    app.use(favicon(path.join(__dirname, "public", "favicon.ico")));

    app.use(vhost("f.uwu.cam", useSite("f.uwu.cam")));
    app.use("/s/:id", useSite());
    app.use("/uploads", express.static(uploadsDir));
    // index page
    app.get("/", useSite());
    app.post("/upload", (req, res) => {
        upload(req, res, (err) => {
            if (err) res.render("uploader/index", { msg: err });
            else {
                if (req.file === undefined)
                    return res.render("uploader/index", {
                        msg: "Error: No files selected!",
                    });
                else
                    res.render("uploader/index", {
                        msg: "File uploaded.",
                        file: `uploads/${req.file.filename}`,
                    });
            }
        });
    });

    app.use(express.static(path.join(__dirname, "public")));
    app.set("view engine", "pug");
    app.set("views", path.join(__dirname, "pages"));

    app.use((_, res) => res.send("404 not found."));

    return app;
};
