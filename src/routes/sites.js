export const sendSite = (res, id) => {
    switch (id) {
        case "f.uwu.cam":
            return res.render("uploader/index");
        default:
            return res.render("index");
    }
};

export const useSite = (id) => (req, res) =>
    sendSite(res, id || req.params.id || "");
