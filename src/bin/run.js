const app = require("../server.js")(process.argv);
import { config } from "../config";

const port = config.toObject().port;

app.listen(port, () => console.log(`Server listening on port ${port}`));
